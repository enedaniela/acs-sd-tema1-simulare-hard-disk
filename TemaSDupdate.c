#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct nod
{
	int damage;
	unsigned  int data;
	int index;
	int linie;
	struct nod *next;
}nod;

typedef struct ListD
{
	int line_sector;
	struct nod *sector;
	struct ListD *urm;
	struct ListD *pred;
}ListD;

struct node
{
    char cmd;
    int line;
    int indexLine;
    unsigned int data;
    struct node *ptr;
}*front,*rear,*temp,*front1,*e;

struct nodes
{
    char cmd;
    int line;
    int indexLine;
    unsigned int data;
    struct nodes *ptr;
}*tops,*tops1,*temps, *es;
int t, exec = 1, done;
FILE *fid;


void initHardDisk(ListD **HD)
{

	(*HD) = NULL;
}

/* Aici adaugam cate un nod in lista dublu inlantuita (care este de fapt hard-ul nostru) */

void  adaugLista(ListD **HD, nod *line, int i)
{

	if((*HD) == NULL)		// daca hardDisk-ul este gol
	{
		(*HD) = (ListD*)malloc(sizeof(ListD));
		(*HD)->urm = NULL;
		(*HD)->pred = NULL;
		(*HD)->sector = line;
	}
	else
	{
		ListD *newNode = (ListD*)malloc(sizeof(ListD));//aloc memorie pentru nodul pe care vreau sa il adaug
		ListD *aux = (ListD*)malloc(sizeof(ListD));

		newNode->urm = NULL;
		newNode->sector = line;// il fac sa pointeze catre lista circulara
		newNode->line_sector = i;


		aux = (*HD);            // acesta este i-ul in listele inlantuite

		while(aux->urm != NULL)// ma pozitionez pe ultimul nod din lista inainte de adaugare
		{
            aux = aux->urm;
		}

		newNode->pred = aux;//fac legaturile
		aux->urm = newNode;

	}

}
/*Functie ce adauga celule in listele circulare(liniile Hard Disk-ului)*/

void adaugaCelula(nod **first, int j, int i)// trimit ca parametrii lista si indicele celulei
{
	nod *temp, *aux;

	temp = (nod *)malloc(sizeof(nod));// celula pe care vreau sa o adaug
	//completez datele
	temp->data = 0;
	temp->index = j;
	temp->damage = 0;
	temp->linie = i;

	if((*first) == NULL)// daca lista circulara e vida
	{
		temp->next = temp;//celula ce pointeaza spre ea insasi
		(*first) = temp;
	}
	else
	{
		aux = (*first);

		while(aux->next != (*first))// cat timp nu m-am intors la celula initiala
			aux = aux->next;//parcurg lista pana la ultimul nod inainte de adaugare


        temp->next = (*first);// noul nod pointeaza catre inceputul listei
		aux->next = temp;//ultimul nod inainte de adaugare pointeaza catre noul nod introdus

	}

}

/* Aceasta functie nu este neaparat necesara, dar poate fi utila in unele cazuri. Ce anume face: afisaza continutul
unei anumite linii din hardDisk (linie pe care o transmiti ca parametru)*/

void printList(nod *lista)
{
	nod* aux = lista;

	//printf("%d ", aux->index);

	aux = aux->next;

	while(aux != lista)
	{
		//printf("index = %d ; damage = %d ; data =  %04X ;", aux->index, aux->damage, aux->data);
		//printf("\n");

		aux = aux->next;
	}

}

/* Aici construim hardDisk-ul: intr-un for pana la numarul de linii alocam memorie pentru un nou cap de lista circulara
(pentru liniile hardDisk-ului), dupa care incepem sa construim efectiv liniile in for-ul din interior */

void buildHardDisk(ListD **HD, int nrLines)
{
	int i = 0, j;

	nod *cap;

	for(i = 0; i < nrLines; i++)
	{

		cap = (nod*)malloc(sizeof(nod));
		cap = NULL;

		for(j = 0; j < pow(2,(i+4)); j++)
		{
				adaugaCelula(&cap,j, i);
		}

		adaugLista(HD, cap,i);
	}

}

/* In aceasta functie printez linia hardDisk-ului, care este o lista circulara simplu inlantuita, deci pentru
a o afisa pe ecran, printez mai intai primul element, apoi trec aux-ul (i-ul) la urmatorul element si incep
sa printez normal in while toate nodurile listei */


void printLine(nod *line)
{
    nod *aux = line;

	printf("index = %d ; damage =  %d ; data =  %04X ;" , aux->index, aux->damage, aux->data);/* printez primul element separat
	pentru ca la list circulara conditia de iesire din while este ca nodul printat sa fie diferit de primul nod*/
    printf("\n");

    aux = aux->next;

	while(aux != line)
	{
		printf("index = %d ; damage = %d ; data =  %04X ;", aux->index, aux->damage, aux->data);
		printf("\n");

		aux = aux->next;
	}
}

/* Aici parcurg hardDisk-ul. Practic merg pe locatiile 0 liniilor si afisez fiecare linie
cu functie printLine */

void printHardDisk(ListD *HD, int nrLines)
{

	ListD *aux = HD;
	int i = 0;

	while(aux != NULL && i < nrLines)
	{	printf("Linia %d\n",aux->line_sector);
		printLine(aux->sector);//printez lista circulara

        printf("\n");

		aux = aux->urm;// trec la urmatoarul nod al listei dublu inlantuita, adica la urmatoarea lista circulara

		i++;

	}


}


void readData(nod **cursor)
{

	printf("%04X\n",(*cursor)->data);
	(*cursor)->damage+=6;
}

void writeData(nod **cursor, unsigned int d)
{
	//printf("%04X",d);
	(*cursor)->data = d;
	(*cursor)->damage+=31;
}

void readDamage(nod **cursor)
{
	printf("%d\n",(*cursor)->damage);
	(*cursor)->damage+=3;
}

void create()
{
    front = rear = NULL;
}

void enq(char c, int l, int idx, unsigned int data)
{
    if (rear == NULL)
    {
        rear = (struct node *)malloc(1*sizeof(struct node));
        rear->ptr = NULL;
        rear->cmd= c;
        rear->line = l;
        rear->indexLine = idx;
        if(rear->cmd == 'w')
        rear->data = data;
        front = rear;
    }
    else
    {
        temp=(struct node *)malloc(1*sizeof(struct node));
        rear->ptr = temp;
        temp->cmd = c;
        temp->line = l;
        temp->indexLine = idx;
         if(temp->cmd == 'w')
        temp->data = data;
        temp->ptr = NULL;
 
        rear = temp;
    }

}
 
/* Displaying the queue elements */
void display()
{
    front1 = front;
 
    if ((front1 == NULL) && (rear == NULL))
    {
        printf("Queue is empty");
        return;
    }
    while (front1 != rear)
    {
        if (front1->cmd == 'w')
            printf("\nCommand to be executed %c on line %d and index %d write dates %04X", front1->cmd,front1->line,front1->indexLine,front1->data);
        else
            printf("\nCommand to be executed %c on line %d and index %d ", front1->cmd,front1->line,front1->indexLine);
        front1 = front1->ptr;

    }
    if (front1 == rear)
    {
        if (front1->cmd == 'w')
            printf("\nCommand to be executed %c on line %d and index %d write dates %04X", front1->cmd,front1->line,front1->indexLine,front1->data);
        else
        printf("\nCommand to be executed %c on line %d and index %d ", front1->cmd,front1->line,front1->indexLine);
    }
}
 
/* Dequeing the queue */
void deq()
{
    front1 = front;
 
    if (front1 == NULL)
    {
        printf("\n Error: Trying to display elements from empty queue");
        return;
    }
    else
        if (front1->ptr != NULL)
        {
            front1 = front1->ptr;
            free(front);
            front = front1;
        }
        else
        {
            free(front);
            front = NULL;
            rear = NULL;
        }
       
}
 
/* Returns the front element of queue */
struct node * frontelement()
{
    if ((front != NULL) && (rear != NULL))
        return front;
    else
        return NULL;
}
 
/* Display if queue is empty or not */
int empty()
{
     if ((front == NULL) && (rear == NULL))
        return 1;
    else
       return 0;
}

/* Create empty stack */
void creates()
{
    tops = NULL;
}
/* Push data into stack */
void push(char c, int l, int idx, unsigned int data)
{
    if (tops == NULL)
    {
        tops =(struct nodes *)malloc(1*sizeof(struct nodes));
        tops->ptr = NULL;
        tops->cmd= c;
        tops->line = l;
        tops->indexLine = idx;
        if(tops->cmd == 'w')
        tops->data = data;
    }
    else
    {
        temps =(struct nodes *)malloc(1*sizeof(struct nodes));
        temps->ptr = tops;
        temps->cmd = c;
        temps->line = l;
        temps->indexLine = idx;
        if(temps->cmd == 'w')
        temps->data = data;
        tops = temps;
    }
   
}
 
/* Display stack elements */
void displays()
{
    tops1 = tops;
 
    if (tops1 == NULL)
    {
        printf("Stack is empty");
        return;
    }
 
    while (tops1 != NULL)
    {
        if (tops1->cmd == 'w')
            printf("\nCommand to be executed %c on line %d and index %d write dates %04X", tops1->cmd,tops1->line,tops1->indexLine,tops1->data);
        else
            printf("\nCommand to be executed %c on line %d and index %d ", tops1->cmd,tops1->line,tops1->indexLine);
        tops1 = tops1->ptr;
    }
 }
 
/* Pop Operation on stack */
void pop()
{
    tops1 = tops;
 
    if (tops1 == NULL)
    {
        printf("\n Error : Trying to pop from empty stack");
        return;
    }
    else
        tops1 = tops1->ptr;
    //printf("Command %c line %d index %d data %04X", tops->cmd, tops->line, tops->indexLine, tops->data);
    free(tops);
    tops = tops1;
   
}
 
/* Return tops element */
struct nodes * topselement()
{
    return(tops);
}
 
/* Check if stack is empty or not */
int emptys()
{
    if (tops == NULL)
        return 1;
    else
        return 0;
}

void readCmd(int what)
{
	char c;
	int idx, l;
	unsigned int data;
	if (exec == 0)
		return;
				fscanf(fid, "::%c", &c);
		   			//printf("%c ", c);
				    switch (c)
				    {
				    	case 'r':
				    	case 'd':
				    	fscanf(fid,"%d %d\n", &l, &idx);
				    	//printf("%d %d\n", l, idx);
				    	break;

				    	case 'w':
				    	fscanf(fid,"%d %d %04X\n", &l, &idx, &data);
				    	//printf("%d %d %04X\n", l, idx, data);
				    	break;

				    	case 'e':
				    	exec = 0;
				    	return;
				    
				    }
			fscanf(fid,"%d\n",&t);
			if(what == 1)
				enq(c, l, idx, data);
			if(what == 2)
				push(c, l, idx, data);

}
void executeCmd(nod **cursor, char c, int l, int idx, unsigned data,int what)
{
	if(what == 2 && !exec)
		return;
	if(what == 2 && !done)
		return;
	t--;
	if(what == 1 && t==0 && exec)
		readCmd(what);

	if(c == 'r')
		readData(cursor);
	if(c == 'w')
		writeData(cursor,data);
	if(c == 'd')
		readDamage(cursor);
	if(what == 2 && t == 0 && exec)
	{
		done = 0;
		pop();
		readCmd(what);
	}

}
void moveOnLevel(ListD **myharddisk,nod **cursor, int l, int what)
{
	if(what == 2 && !exec)
		return;
	if(what == 2 && !done)
		return;


	while((*myharddisk)->line_sector > l)
	{
		(*myharddisk) = (*myharddisk)->pred;
		(*myharddisk)->sector->damage++;
		t--;
		if(t==0 && exec)
		{
			readCmd(what);
			if(what == 2)
			{
				(*cursor) = (*myharddisk)->sector;
				done = 0;
				return;
			}
		}
	}

	while((*myharddisk)->line_sector < l)
	{
		(*myharddisk) = (*myharddisk)->urm;
		(*myharddisk)->sector->damage++;
		t--;
		if(t==0 && exec)
		{
			readCmd(what);
			if(what == 2)
			{
				(*cursor) = (*myharddisk)->sector;
				done = 0;
				return;
			}
		}
	}
	(*cursor) = (*myharddisk)->sector;
}

void moveOnLine(ListD **myharddisk, nod **cursor, int idx, int what)
{

	if(what == 2 && !exec)
		return;
	if(what == 2 && !done)
		return;
	while((*cursor)->index != idx)
	{
		(*cursor) = (*cursor)->next;
		(*cursor)->damage++;
		t--;
		if(t==0 && exec)
		{
			readCmd(what);
			if(what == 2)
			{
				done = 0;
				return;
			}
		}
	}

}


void execute(ListD **myharddisk, nod **cursor, int what)
{
	(*cursor) = (*myharddisk)->sector;
	if(what == 1)
	{
	readCmd(what);
	while (exec || empty() == 0) 
	{
	    if(empty() == 1)
	    {
	    	(*cursor)->damage+=t;
	   		 t=0;
	   		 if(exec)
	   		 	readCmd(what);

	    }
	    if(empty() && !exec)
	    	break;

	    e = frontelement();
			if(e->line != (*myharddisk)->line_sector)
			{
				moveOnLine(myharddisk,cursor,0,what);
				moveOnLevel(myharddisk,cursor,e->line,what);
			}
				moveOnLine(myharddisk,cursor,e->indexLine,what);
		
			executeCmd(cursor,e->cmd,e->line,e->indexLine,e->data,what);
			deq();
	}
	return;
			
	}
	exec = 1;
	readCmd(what);
	while(exec)
	{
		if(emptys())
		{
			(*cursor)->damage+=t;
	   		 t=0;
	   		 if(exec)
	   		 	readCmd(what);
		}
		if(!exec)
			break;
		es = topselement();
		done =1;
		if(es->line != (*myharddisk)->line_sector)
			{
				moveOnLine(myharddisk,cursor,0,what);
				moveOnLevel(myharddisk,cursor,es->line,what);
			}
				moveOnLine(myharddisk,cursor,es->indexLine,what);
		
			executeCmd(cursor,es->cmd,es->line,es->indexLine,es->data,what);
			if(done)
				pop();
	}
	while(!empty())
		deq();
	while(!emptys())
		pop();
			
}

void damageAvarage(ListD *myharddisk, int nrl)
{
	int i = 0, j, nr_elem = 0;
	float d1=0, d2=0, d3=0, d4=0; nr_elem = 0;
	nod * cursor = myharddisk->sector;
	while(myharddisk != NULL)
	{
		cursor = myharddisk->sector;
		for(j = 0; j < pow(2,i+4)/4; j++)
		{
			//printf("%d ",cursor->damage)
			d1 = d1 + cursor->damage;
			cursor = cursor->next;
			nr_elem++;
			
		}

		for(j = pow(2,i+4)/4; j < pow(2,i+4)/2; j++)
		{
			d2 = d2 + cursor->damage;
			cursor = cursor->next;
		}

		for(j = pow(2,i+4)/2; j < pow(2,i+4)*3/4; j++)
		{
			d3 = d3 + cursor->damage;
			cursor = cursor->next;
		}

		for(j = pow(2,i+4)/4; j < pow(2,i+4)/2; j++)
		{
			d4 = d4 + cursor->damage;
			cursor = cursor->next;
		}
		myharddisk = myharddisk->urm;
		i++;
		
	}
	
	d1 = d1/nr_elem - 0.0049;
	d2 = d2/nr_elem - 0.0049;
	d3 = d3/nr_elem - 0.0049;
	d4 = d4/nr_elem - 0.0049;

	printf("%.2f %0.2f %0.2f %.2f\n", d1, d2, d3, d4);
}


int main(int argc,char **argv)
{
	ListD *myharddisk,*aux;
	aux = myharddisk;
	nod *cursor = myharddisk->sector;
	int nrl,what;
	fid = fopen(argv[1],"r");
	freopen(argv[2],"w",stdout);

	initHardDisk(&myharddisk);

	fscanf(fid,"%d %d\n", &what, &nrl);

    buildHardDisk(&myharddisk, nrl);
    aux = myharddisk;
   //printHardDisk(myharddisk, nrl);
    execute(&myharddisk,&cursor,what);
    myharddisk=aux;
    damageAvarage(myharddisk, nrl);
    fclose(fid);
    fclose(stdout);
    return 0;

}
